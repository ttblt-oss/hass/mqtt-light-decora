#!/usr/bin/python

import decora
import sys

address = sys.argv[1]
switch = decora.decora(address, None)
rawkey = switch.read_key()
if rawkey == b"LEVI":
    print("Switch is not in pairing mode - hold down until green light flashes\n")
else:
    key = rawkey[0] << 24 | rawkey[1] << 16 | rawkey[2] << 8 | rawkey[3]
    print(hex(key))
