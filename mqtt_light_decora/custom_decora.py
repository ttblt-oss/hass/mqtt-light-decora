"""Custom Decora module to handle multiple bluetooth device."""
from bluepy import btle
import decora


class CustomDecora(decora.decora):
    """Custom Decora class to handle multiple bluetooth device."""

    def __init__(self, mac, key=None, iface=0):
        """Constructor."""
        decora.decora.__init__(self, mac, key)
        self.iface = iface

    def _connect(self):
        self.device = btle.Peripheral(self.mac, addrType=btle.ADDR_TYPE_PUBLIC, iface=self.iface)
        try:
            characteristics = self.device.getCharacteristics()
        except btle.BTLEException:
            raise decora.decoraException("Unable to connect")
        self.handles = {}
        for characteristic in characteristics:
            if characteristic.uuid == "0000ff01-0000-1000-8000-00805f9b34fb":
                self.handles["state"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff02-0000-1000-8000-00805f9b34fb":
                self.handles["config1"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff03-0000-1000-8000-00805f9b34fb":
                self.handles["config2"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff04-0000-1000-8000-00805f9b34fb":
                self.handles["location1"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff05-0000-1000-8000-00805f9b34fb":
                self.handles["location2"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff06-0000-1000-8000-00805f9b34fb":
                self.handles["event"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff07-0000-1000-8000-00805f9b34fb":
                self.handles["time"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff08-0000-1000-8000-00805f9b34fb":
                self.handles["data"] = characteristic.getHandle()
            if characteristic.uuid == "0000ff09-0000-1000-8000-00805f9b34fb":
                self.handles["name"] = characteristic.getHandle()
